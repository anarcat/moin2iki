MoinMoin to ikiwiki converter
=============================

This program converts MoinMoin wikis to ikiwikis backed by a git
repository, including full history. It parses the wiki pages into
Markdown using the MoinMoin engine.

Bug reports and a copy of the git repository of this project are
available on the [project page](https://gitlab.com/anarcat/moin2iki/). There is also a page for the
project on the [Ikiwiki site](http://ikiwiki.info/tips/convert_moinmoin_to_ikiwiki), including a discussion page.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-generate-toc again -->
**Table of Contents**

 - [Requirements](#requirements)
 - [Usage](#usage)
 - [Supported features](#supported-features)
 - [Known issues](#known-issues)
 - [Credits](#credits)

<!-- markdown-toc end -->

Requirements
------------

To use these scripts to convert a MoinMoin wiki to ikiwiki, you will
need:

 * Perl
 * Python
 * An installation of MoinMoin code and required modules, e.g.

        sudo apt-get install --no-install-recommends python-moinmoin

 * A copy of your MoinMoin data (assumed in `moinmoin` below)
 * Optionally, an `--authorsfile` mapping MoinMoin users to how they should appear
   in the Git repository.  Lines in an authors file should look like this:

        MoinUserName=Full Name <email.address@example.org>

Usage
-----

The software is made of two pieces:

 * the importer (`moin2git`) - which converts the wiki pages into a git repository with full history
 * the converter (`moin2mdwn`) - which converts a set of moin-formatted text files into markdown + ikiwiki directives

Use those commands to convert the MoinMoin wiki to git:

    git init ikiwiki
    cd moinmoin
    moin2git | ( cd ../ikiwiki && git fast-import )

If the importer fails, you can try to import just that page again with:

    moin2git -p PageName | ( cd ../ikiwiki && git fast-import )

The script has a detailed usage available under --help. It can be ran
multiple times and will import changes incrementally.

The above will generate a full git repository with one text file per
page, in MoinMoin markup format. To convert that to Ikiwiki-compatible Markdown, you need to run the conversion script:

    cd moinmoin
    moin2mdwn -r ../ikiwiki

Similarly, if this fails you can retry only with one page with the -p option.

Obviously, if you do not use the same options for selecting pages (-p,
-D or -u) for both scripts, the second will fail.

The script will raise exceptions if it encounters unsupported macros,
you can use --force to skip those problems if you wish, to complete
the conversion anyways.

You will likely want to commit that conversion to git, assuming it's
successful, for this, use:

    cd ikiwiki
    git add .
    find . -name '*.moin' -print0 | xargs -0 git rm
    git commit -m"convert to markdown"

Then the final step is to import that git repo into ikiwiki, which is
out of the scope of this modest manual. However, as a hint, you could
simply try to convert those markdown pages to HTML using the simplest
ikiwiki invocation:

    ikiwiki ikiwiki public_html

Make sure you enabled the plugins for the directives mentionned at the
end of the script, example:

    ikiwiki --plugin calendar --plugin goodstuff --plugin map --plugin inline --plugin toc --plugin pagecount --plugin orphans ikiwiki public_html.html

Supported features
------------------

### MoinMoin importer

 * supports latest MoinMoin versions (tested with 1.9.x)
 * uses `git fast-import` to improve performance (10 minutes and 200M of ram for a 7 years old 2GB Moinmoin wiki)
 * multistep process allows bulk edit through git before markdown conversion, or staying with a 
 * imports attachments as subpages
 * uses the per-page edit log
 * consistent: multiple runs will generate the same repository
 * re-entrant: can be run multiple times to import new changes

### MoinMoin converter

 * most of the inline markup
 * links
 * attachment links
 * smileys
 * images (not well tested), into [ikiwiki/directive/img](https://ikiwiki.info/ikiwiki/directive/img)
 * preformatted and code areas, including [ikiwiki/directive/format](https://ikiwiki.info/ikiwiki/directive/format)
 * ordered, unordered and definition lists
 * tables (although only with HTML and no styles)

#### Supported macros

 * TableOfContents, through [ikiwiki/directive/toc](https://ikiwiki.info/ikiwiki/directive/toc)
 * Navigation, through [ikiwiki/directive/map](https://ikiwiki.info/ikiwiki/directive/map) (so as a nested
   vertical list instead of an horizontal list)
 * PageList, through [ikiwiki/directive/map](https://ikiwiki.info/ikiwiki/directive/map)
 * MonthCalendar, partially, through [ikiwiki/directive/calendar](https://ikiwiki.info/ikiwiki/directive/calendar)
 * FootNote, through multimarkdown (`[^foo]` → `[^foo]: this is the footnote`)
 * Anchor, through markdown and plain HTML
 * `<<BR>>`, through the weird line ending thing
 * AttachList, through a weird [ikiwiki/directive/inline](https://ikiwiki.info/ikiwiki/directive/inline)
 * FullSearch, partially, only through [ikiwiki/directive/inline](https://ikiwiki.info/ikiwiki/directive/inline) (so no textual search)
 * Include, partially through [ikiwiki/directive/inline](https://ikiwiki.info/ikiwiki/directive/inline) (so missing boundary extraction and heading level generation)
 * PageCount, same name even :)
 * OrphanedPages, through [ikiwiki/directive/orphans](https://ikiwiki.info/ikiwiki/directive/orphans)
 * Date and Datetime, should be through [plugins/date](https://ikiwiki.info/plugins/date) instead of
   current hack

#### Supported parsers

 * the main "moin wiki" markup
 * highlight parser, through the [plugins/format](https://ikiwiki.info/plugins/format) plugin
 * other parsers may be supported if an equivalent plugin exists in Ikiwiki (example: [plugins/rst](https://ikiwiki.info/plugins/rst))

Known issues
------------

This script is being used to test the conversion of the venerable [Koumbit wiki](https://wiki.koumbit.net/) into Ikiwiki, and so far progress is steady but difficult. The current blocker is:

 * figuring out exactly which pages should exist and which should not, as there is ambiguity in the internal datastructures of MoinMoin, which become apparent when running the conversion script, as files a missing

### Todos

There are also significant pieces missing:

 * inline parsers and hackish styled tables
 * turn categories into tags
 * name converted page to the right name depending on the `#format` parameter on top of page
 * finish a full converter run on the Koumbitwiki
 * improve the output of the converter (too much debugging)

### MoinMoin features missing from ikiwiki

The importer is pretty much complete, but the converter can only go so far as what features ikiwiki supports. Here are the MoinMoin features that are known to be missing from ikiwiki. Note that some of those features are available in MoinMoin only through third-party extensions.

 * [todo/do_not_make_links_backwards/](https://ikiwiki.info/todo/do_not_make_links_backwards/) - MoinMoin and Creole use `\[[link|text]]`, while ikiwiki uses `\[[text|link]]` - for now the converter generates [[markdown]] links so this is not so much an issue, but will freak out users
 * [todo/internal_definition_list_support/](https://ikiwiki.info/todo/internal_definition_list_support/) - includes tabling the
   results ([MoinMoin's DictColumns macro](http://moinmo.in/MacroMarket/DictColumns)), could be approximated
   with the [field](https://ikiwiki.info/plugins/contrib/field/) and [report](https://ikiwiki.info/plugins/contrib/report/) plugins
 * [todo/per page ACLs](https://ikiwiki.info/todo/per_page_ACLs) - ([MoinMoin's ACLs](http://moinmo.in/HelpOnAccessControlLists))
 * [MailTo](http://moinmo.in/HelpOnMacros/MailTo) macro spam protection
 * list pages based on full text page search
 * extract part of other pages with the inline macro
 * specifying a template when creating a page (as opposed to matching a pagespec)
 * specifying a style for a sub-section (MoinMoin's inline parsers
   allow the user to specify a CSS class - very useful see
   [the documentation](http://moinmo.in/HelpOnMoinWikiSyntax#Using_the_wiki_parser_with_css_classes)
   to get an idea)
 * the above also keeps the SectionParser from being properly supported
 * regex matching all over the place: pagespec, basically, but all
   full text search (which is missing anyways, see above)

### Missing macros

 * RandomPage(N) - lists N random pages, skipped
 * Gallery() - skipped
 * Gettext - translates the string accordign to internal translation
   system, ignored
 * AdvancedSearch - an elaborate search form provided by MoinMoin
 * Goto - a simple "jump to page" macro

Credits
-------

The converter was originally written by Jamey Sharp and
[JoshTriplett](https://ikiwiki.info/users/joshtriplett/), with contributions from John Goerzen and Jonathan
Grimm. It originally included support for Tikiwiki, by parsing the
wiki pages into HTML and then converting back into Markdown using the
`libhtml-wikiconverter` Perl package. That original version is still
available from [Josh's wiki page](/users/JoshTriplett).

This version is a complete rewrite with support only for MoinMoin, by
Antoine Beaupré.
